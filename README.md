# Gut scoring

This software tool combines gut feeling and objective criteria for evaluating students.



## Usage

To use this tool, you need to perform three steps. First you need to fill in the necessary data of your assessment in the scores `.csv` template. Next, you need to run the script that will perform the optimisation for you. Finally, the optimised scores will be available in a new `.csv` file.



### 1. Fill in the scores template

Make a copy of the `poem-scores.csv`. You need to enter three types of data:

1. (blue) list the scoring criteria and assign weights

2. (red) score each student according to these criteria

3. (green) add for each student an intuitive score according to your gut feeling



![](template-guide.png)



Save the `.csv` file, e.g. `rocket-project.csv`.



### 2. Run the optimisation script

Now run the optimisation script with python. You need to add the correct `.csv` file as an argument: `python optimise.py -f rocket-project.csv`. The script will show a report of the criteria with their old and new weights.

If you think the weights differ too much or too little from their initial values, you can modify this behaviour. The script accepts a `-a` float parameter that tunes how aggressively it will alter the weights. Some examples

* `python optimise.py -f rocket-project.csv -a 10000` (or any large value) will allow no altering of the weights
* `python optimise.py -f rocket-project.csv -a 0` will allow as much variation as necessary
* `python optimise.py -f rocket-project.csv -a 1.6` will allow some variation

Test with various values for `a` until you are satisfied.





### 3. Read output

For each run of the script, a second `.csv` file will be written to the same directory. Using `rocket-project.csv`, the script will generate `rocket-project-optimised.csv`.

The new `.csv` has the following changes:

* weights are altered
* actual scores are calculated according to these weights



## How does it work?

This problem is an optimisation problem where you want two metrics to be as low as possible:

1. The difference between intuitive scores and the actual scores
2. The difference between the new weights and the initial weights

There is also one constraint: the sum of the optimised weights should be the same as the sum of the initial weights.

This optimisation can be modelled using a cost function:

$`C = a (W_N - W_I)^2 + (S_A - S_I)^2 + l (\sum W_N -t)^2`$

**where**

$`a`$ = parameter for tuning the deviation of weigths from their initial values

$`W_N`$ = ew/optimised weights = the unknown in this problem

$`W_I`$ = initial weights (blue in .csv)

$`S_A`$ = actual scores according to weights = $`Z W_N`$ where $`Z`$ = student scores (red in .csv)

$`S_I`$ = intuitive scores according to gut feeling (green in .csv)

$`l`$ = very large number too make sure this constraint is satisfied

$`t`$ = sum of the initials weights, $`\sum W_I`$


This cost function is a quadratic function in the unknown $`W_N`$ of the form $`W_N^T H W_N + C W_N + C_0`$.

The minimum can be found by checking where the derivative function is zero: $` 2 H W_N + C = 0`$. This is a system of equations that can be solved exactly for $`W_N`$.



## Acknowledgments

Thanks to Matthias Baert for showing how the optimisation could be solved using a cost function.