import numpy as np
import click
from prettytable import PrettyTable

@click.command()
@click.option('-a', default=1.0,
    help='Large values for `a` give a solution close to the initial weights. `a`=0 will ignore initial weights. Default 1.')
@click.option('-f', 'file', default='poem-scores.csv', type=click.File('rb'),
    help='Source file with the student scores. Default scores.csv.')
def optimise(a, file):
    # load inputs
    data = np.matrix(np.genfromtxt(file, delimiter=",", filling_values=0, skip_header=1))
    Z = np.transpose(data[2:, 2:])
    Wi = data[2:,1]
    posMask = np.transpose((Wi>=0).astype(int))
    Si = np.transpose(data[0,2:])
    l = 100000 # must be satisfied!

    # optimalisation
    [nStudents, nWeights] = Z.shape
    sumWeights = np.sum(posMask*Wi)
    numPosWeights = np.sum(posMask)
    H = a*np.identity(nWeights) + np.transpose(Z)*Z + l*np.transpose(posMask)*posMask
    C = -2*a*Wi -2*np.transpose(Z)*Si -2*l*sumWeights*np.transpose(posMask)

    # solve for optimal weights
    Wn = np.linalg.solve(2*H, -C)

    # report
    cost = a*square(Wn-Wi) + square(Z*Wn-Si) + l*square(posMask*Wn-sumWeights)
    print('Cost: ', format_weight(cost, 0))
    show_report(Wi, Wn, file.name)

def square(matrix):
    return np.transpose(matrix)*matrix

def show_report(Wi, Wn, file_name):
    t = PrettyTable(['criterion', 'W_I', 'W_N', '% change'])
    t.align['criterion'] = 'l'

    change = relative_change(Wi, Wn)
    criteria = criteria_names(file_name)

    for i in range(len(Wi)):
        t.add_row([criteria[i], format_weight(Wi, i), format_weight(Wn, i), format_weight(change, i)])

    print(t)


def relative_change(Wi, Wn):
    return list(map(lambda x,y: (y-x)/x*100, Wi, Wn))


def criteria_names(file_name):
    f = open(file_name, 'r')
    return list(map(lambda x: x.split(',')[0], f.readlines()[3:]))


def format_weight(weight_list, index):
    return round(float(weight_list[index]), 2)


if __name__ == '__main__':
    q = optimise()

